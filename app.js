var express = require('express');
var app = express();

var port = process.env.PORT || 5000;

var navArr = [
    {
        Link: '/first',
        Text: 'Pierwszy'
            },
    {
        Link: '/second',
        Text: 'Drugi'
            },
    {
        Link: '/third',
        Text: 'Trzeci'
            }];

var firstRouter = require('./src/routes/firstRoutes')(navArr);
var secondRouter = require('./src/routes/secondRoutes');
var thirdRouter = require('./src/routes/thirdRoutes');


app.use(express.static('public'));
//app.use(express.static('src/views'));
app.set('views', './src/views');
app.set('view engine', 'ejs');

app.get('/', function (request, response) {
    console.log(request);
    response.render('index', {
        title: 'Tytuł',
        nav: navArr
    });
});

app.listen(port, function (e) {
    console.log('Serwer działa na porcie:' + port);
});

app.use('/first', firstRouter);
app.use('/second',secondRouter);
app.use('/third',thirdRouter);   