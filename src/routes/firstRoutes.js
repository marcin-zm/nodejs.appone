var express = require('express');
var firstRouter = express.Router();

var router = function (navArr) {
    var firstData = [
        {
            Id: 1,
            Name: "Name 1",
            Desc: "Description 1"
        },
            {
                Id: 1,
                Name: "Name 2",
                Desc: "Description 2"
        },
            {
                Id: 1,
                Name: "Name 3",
                Desc: "Description 3"
        },
    ];

    firstRouter.route('/')
        .get(function (request, response) {
            response.render('firstView', {
                title: "First",
                nav: navArr,
                data: firstData
            });
        });
    firstRouter.route('/details/:id')
        .get(function (request, response) {
            var id = request.params.id;
            response.render('firstDetailView', {
                title: 'Details:' + firstData[id].Name,
                nav: navArr,
                data: firstData[id]
            });
        });
    return firstRouter;
}
module.exports = router;