var express = require('express');
var secondRouter = express.Router();

secondRouter.route('/')
    .get(function(request,response){
        response.send('Response two'); 
});

module.exports = secondRouter;